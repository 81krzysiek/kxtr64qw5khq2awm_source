<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Interfaces\Http\Front\Action\ImageFormAction::class);

Route::post('/image/upload', \App\Interfaces\Http\ApiInternal\Action\ImageUploadAction::class)->name('web-image-upload');
Route::post('/image/resize', \App\Interfaces\Http\ApiInternal\Action\ImageResizeAction::class)->name('web-image-resize');
Route::post('/worker/status', \App\Interfaces\Http\ApiInternal\Action\WorkerStatusByNameAction::class)->name('web-worker-status');


