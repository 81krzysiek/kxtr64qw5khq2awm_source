<?php
declare(strict_types=1);
namespace App\Interfaces\Console\Jobs;

use App\Domain\Models\Image;
use App\Domain\Models\Worker;
use App\Infrastructure\Commands\Worker\WorkerUpdateCommand;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use ImageResize;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Image $image;
    private int $width;
    private int $height;
    private string $extension;
    private string $newHash;
    private WorkerUpdateCommand $workerUpdateCommand;

    public function __construct(Image $image, int $width, int $height, string $extension, string $newHash, WorkerUpdateCommand $workerUpdateCommand)
    {
        $this->image = $image;
        $this->width = $width;
        $this->height = $height;
        $this->extension = $extension;
        $this->newHash = $newHash;
        $this->workerUpdateCommand = $workerUpdateCommand;
    }

    public function handle(): bool
    {
        try {
            if(Storage::disk('public')->exists($this->image->getFullPath())){

                $imgFile = ImageResize::make(storage_path('app/public/').$this->image->getFullPath());

                $imgFile->resize($this->width, $this->height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($this->image->getFullPathResize($this->newHash, $this->extension));
                $this->workerUpdateCommand->execute($this->newHash, Worker::$STATUS_SUCCESS);

                return true;
            }
            $this->workerUpdateCommand->execute($this->newHash, Worker::$STATUS_ERROR);

            return false;

        }catch (\Throwable $exception){
            $this->workerUpdateCommand->execute($this->newHash, Worker::$STATUS_ERROR);

            throw $exception;
        }
    }
}
