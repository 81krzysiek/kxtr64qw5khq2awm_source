<?php
declare(strict_types=1);
namespace App\Interfaces\Http\Front\Action;

use App\Interfaces\Http\BaseAction;

class ImageFormAction extends BaseAction
{
    public function __invoke()
    {
        return view('welcome');
    }
}
