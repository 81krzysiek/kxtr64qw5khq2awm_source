<?php
declare(strict_types=1);
namespace App\Interfaces\Http;

use App\Domain\Models\Image;

class Helpers
{
    /**
     * @return string|null
     */
    static public function html5ExtensionAddDot(): ?string
    {
        try {
            $array = Image::$SERVICE_DICT_VALIDATE_EXTENSION;
            return implode(',', array_map(function ($value){
                return '.'.$value;
            }, $array));

        }catch (\Exception $e){
            return null;
        }
    }
}
