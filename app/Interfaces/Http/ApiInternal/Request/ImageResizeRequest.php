<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiInternal\Request;

use App\Domain\Models\Image;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ImageResizeRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'extension' => 'required|IN:'.implode(',', Image::$SERVICE_DICT_VALIDATE_EXTENSION).'|max:2048',
            'width' => 'required|integer|min:1|max:1280',
            'height' => 'required|integer|min:1|max:1280',
            'hash' => 'required|string|exists:images,hash'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ], 400));
    }
}
