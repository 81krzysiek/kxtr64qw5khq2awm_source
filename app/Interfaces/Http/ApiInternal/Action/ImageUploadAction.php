<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiInternal\Action;

use App\Domain\Models\Image;
use App\Infrastructure\Commands\Image\ImageCreateCommand;
use App\Infrastructure\Services\UploadFileService;
use App\Interfaces\Http\ApiInternal\Request\ImageUploadRequest;

class ImageUploadAction
{
    private UploadFileService $uploadFileService;
    private ImageCreateCommand $imageCreateCommand;

    public function __construct(UploadFileService $uploadFileService, ImageCreateCommand $imageCreateCommand)
    {
        $this->uploadFileService = $uploadFileService;
        $this->imageCreateCommand = $imageCreateCommand;
    }

    public function __invoke(ImageUploadRequest $request)
    {
        $data = $request->validated();
        $dataImg = $this->uploadFileService->execute($data['file']);
        $this->imageCreateCommand->execute($dataImg);
        $dataImg['fullPath'] = asset('storage/upload/'.$dataImg['hash'].'.'.$dataImg['extension']);
        $dataImg['allowedExtension'] = Image::$SERVICE_DICT_VALIDATE_EXTENSION;

        return response()->json(['success' => true, 'message' => '', 'data' => $dataImg], 200);
    }

}
