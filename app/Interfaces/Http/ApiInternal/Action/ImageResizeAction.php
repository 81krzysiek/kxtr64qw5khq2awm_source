<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiInternal\Action;

use App\Infrastructure\Commands\Image\ImageFindByHashCommand;
use App\Infrastructure\Commands\Worker\WorkerCreateCommand;
use App\Infrastructure\Commands\Worker\WorkerFindByNameCommand;
use App\Infrastructure\Commands\Worker\WorkerUpdateCommand;
use App\Interfaces\Console\Jobs\ImageJob;
use App\Interfaces\Http\ApiInternal\Request\ImageResizeRequest;
use App\Interfaces\Http\BaseAction;
use Illuminate\Support\Str;

class ImageResizeAction extends BaseAction
{
    private ImageFindByHashCommand $imageFindByHashCommand;
    private \Illuminate\Bus\Dispatcher $dispatcher;
    private WorkerFindByNameCommand $workerFindByNameCommand;
    private WorkerUpdateCommand $workerUpdateCommand;
    private WorkerCreateCommand $workerCreateCommand;

    public function __construct(
        ImageFindByHashCommand $imageFindByHashCommand,
        \Illuminate\Bus\Dispatcher $dispatcher,
        WorkerFindByNameCommand $workerFindByNameCommand,
        WorkerUpdateCommand $workerUpdateCommand,
        WorkerCreateCommand $workerCreateCommand
    )
    {
        $this->imageFindByHashCommand = $imageFindByHashCommand;
        $this->dispatcher = $dispatcher;
        $this->workerFindByNameCommand = $workerFindByNameCommand;
        $this->workerUpdateCommand = $workerUpdateCommand;
        $this->workerCreateCommand = $workerCreateCommand;
    }

    public function __invoke(ImageResizeRequest $request)
    {
        $data = $request->validated();
        $image = $this->imageFindByHashCommand->execute($data['hash']);
        if($image){
            $newHash = $this->newHash($data['hash'], (int)$data['width'], (int)$data['height'], $data['extension']);
            $this->workerCreateCommand->execute(['name' => $newHash]);
            $this->dispatcher->dispatch(new ImageJob($image, (int)$data['width'], (int)$data['height'], $data['extension'], $newHash, $this->workerUpdateCommand));

            return response()->json(['hash' => $newHash, 'extension' => $data['extension'], 'url' => asset('storage/upload/'.$newHash.'.'.$data['extension'])], 200);
        }
    }

    /**
     * @param string $hash
     * @param int $width
     * @param int $height
     * @param string $extension
     * @return string
     */
    private function newHash(string $hash, int $width, int $height, string $extension): string
    {
        $hash = md5($hash.'_'.$width.'_'.$height.'_'.$extension.(string) Str::uuid().'_'.time());

        if($this->workerFindByNameCommand->execute($hash) === null){
            return $hash;
        }

        return $this->newHash($hash, $width, $height, $extension);
    }
}
