<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiInternal\Action;

use App\Infrastructure\Commands\Worker\WorkerFindByNameCommand;
use App\Interfaces\Http\ApiInternal\Request\WorkerStatusByNameRequest;
use App\Interfaces\Http\BaseAction;

class WorkerStatusByNameAction extends BaseAction
{
    private WorkerFindByNameCommand $workerFindByNameCommand;

    public function __construct(WorkerFindByNameCommand $workerFindByNameCommand)
    {
        $this->workerFindByNameCommand = $workerFindByNameCommand;
    }

    public function __invoke(WorkerStatusByNameRequest $request)
    {
        $data = $request->validated();
        $worker = $this->workerFindByNameCommand->execute($data['name']);

        if($worker){

            return response()->json(['success' => true, 'message' => '', 'status' => $worker->status], 200);
        }

        return response()->json(['success' => false, 'message' => ''], 410);
    }
}
