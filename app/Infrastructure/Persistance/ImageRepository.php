<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\ImageContract;
use App\Domain\Models\Image;

class ImageRepository implements ImageContract
{
    public function create(array $data): void
    {
        Image::create($data);
    }

    public function getByHash(string $hash): ?Image
    {
        return Image::where('hash', $hash)->first();
    }

}
