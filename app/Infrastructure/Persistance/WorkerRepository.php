<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\WorkerContract;
use App\Domain\Models\Worker;

class WorkerRepository implements WorkerContract
{
    public function create(array $data): void
    {
        Worker::create($data);
    }

    public function findByName(string $name): ?Worker
    {
        return Worker::where('name', $name)->first();
    }

    public function update(string $name, int $status): void
    {
        Worker::where('name', $name)->update(['status' => $status]);
    }

}
