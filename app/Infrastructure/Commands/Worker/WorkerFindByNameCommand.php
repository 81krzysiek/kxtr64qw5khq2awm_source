<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Worker;

use App\Domain\Contracts\WorkerContract;
use App\Domain\Models\Worker;

class WorkerFindByNameCommand
{
    private WorkerContract $contract;

    public function __construct(WorkerContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $name
     * @return Worker|null
     */
    public function execute(string $name): ?Worker
    {
        return $this->contract->findByName($name);
    }
}
