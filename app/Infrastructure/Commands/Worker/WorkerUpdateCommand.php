<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Worker;

use App\Domain\Contracts\WorkerContract;

class WorkerUpdateCommand
{
    private WorkerContract $contract;

    public function __construct(WorkerContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $name
     * @param int $status
     * @return void
     */
    public function execute(string $name, int $status): void
    {
        $this->contract->update($name, $status);
    }
}
