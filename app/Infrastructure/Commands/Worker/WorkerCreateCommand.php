<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Worker;

use App\Domain\Contracts\WorkerContract;

class WorkerCreateCommand
{
    private WorkerContract $contract;

    public function __construct(WorkerContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     * @return void
     */
    public function execute(array $data): void
    {
        $this->contract->create($data);
    }
}
