<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Image;

use App\Domain\Contracts\ImageContract;
use App\Domain\Models\Image;

class ImageCreateCommand
{
    private ImageContract $contract;

    public function __construct(ImageContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     */
    public function execute(array $data): void
    {
        $this->contract->create($data);
    }
}
