<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Image;

use App\Domain\Contracts\ImageContract;
use App\Domain\Models\Image;

class ImageFindByHashCommand
{
    private ImageContract $contract;

    public function __construct(ImageContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $hash
     * @return Image|null
     */
    public function execute(string $hash): ?Image
    {
        return $this->contract->getByHash($hash);
    }
}
