<?php
declare(strict_types=1);
namespace App\Infrastructure\Services;

use App\Domain\Models\Image;
use App\Infrastructure\Commands\Image\ImageFindByHashCommand;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class UploadFileService
{
    private ImageFindByHashCommand $imageFindByHashCommand;

    public function __construct(ImageFindByHashCommand $imageFindByHashCommand)
    {
        $this->imageFindByHashCommand = $imageFindByHashCommand;
    }

    public function execute(UploadedFile $file)
    {
        $fileName = $this->generateName();
        $extension = $file->getClientOriginalExtension();
        $orgName = $file->getClientOriginalName();
        $filePath = Image::$PATH;

        $file->storeAs($filePath, $fileName.'.'.Str::lower($extension), 'public');

        return [
            'hash' => $fileName,
            'name' => $orgName,
            'extension' => Str::lower($extension),
            'path' => $filePath,
        ];
    }

    /**
     * @return string
     */
    private function generateName(): string
    {
        $name = md5((string) Str::uuid().'_'.time());

        if($this->imageFindByHashCommand->execute($name) !== null){

            return $this->generateName();
        }

        return $name;
    }
}
