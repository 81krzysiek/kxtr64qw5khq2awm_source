<?php
declare(strict_types=1);
namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    public static int $STATUS_WAIT = 0;
    public static int $STATUS_SUCCESS = 1;
    public static int $STATUS_ERROR = 2;
    protected $table = 'workers';

    public $timestamps = true;

    protected $fillable = [
        'name', 'status'
    ];
}
