<?php
declare(strict_types=1);
namespace App\Domain\Models;

use App\Domain\Traits\LowerField;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use LowerField;
    public static array $SERVICE_DICT_VALIDATE_EXTENSION = ['jpg', 'jpeg', 'png'];
    public static string $PATH = 'upload';
    protected $table = 'images';

    public $timestamps = true;

    protected $fillable = [
        'name', 'hash', 'path', 'extension'
    ];

    protected array $lowerFields = ['extension'];

    public function getFullPath(): string
    {
        return $this->path.'/'.$this->hash. '.'. $this->extension;
    }

    public function getFullPathResize(string $newHash, string $extension): string
    {
        return storage_path('app/public/').$this->path.'/'.$newHash.'.'.$extension;
    }
}
