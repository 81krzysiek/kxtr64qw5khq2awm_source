<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Image;

interface ImageContract
{
    public function create(array $data): void;

    public function getByHash(string $hash): ?Image;
}
