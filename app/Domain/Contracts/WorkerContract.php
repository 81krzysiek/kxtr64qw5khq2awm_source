<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Worker;

interface WorkerContract
{
    public function create(array $data): void;

    public function findByName(string $name): ?Worker;

    public function update(string $name, int $status): void;
}
