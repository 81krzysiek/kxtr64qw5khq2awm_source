<?php
declare(strict_types=1);
namespace App\Domain\Traits;

use Illuminate\Support\Str;

trait LowerField
{
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->lowerFields)) {
            $value = Str::lower($value);
        }

        parent::setAttribute($key, $value);
    }
}
