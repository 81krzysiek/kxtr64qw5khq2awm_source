<?php
declare(strict_types=1);
namespace App\Application\Providers;

use App\Domain\Contracts\ImageContract;
use App\Domain\Contracts\WorkerContract;
use App\Infrastructure\Persistance\ImageRepository;
use App\Infrastructure\Persistance\WorkerRepository;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap domain application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register domain application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageContract::class, ImageRepository::class);
        $this->app->bind(WorkerContract::class, WorkerRepository::class);
    }
}
